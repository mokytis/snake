var gridSquareWidth = 20;
var apple;
var snakeHead;
var snakeTail = [];
var gridWidth;
var gridHeight;
var margin = 2;
var direction = "left";
var snakeLength = 5;
var apple;
var previous_direction;
var lengthInc = 5;
var fps = 15;
var startLength = 5;
var appleColor = 'red';
var snakeColor = 255;
var headColor = 255;
var playing = true;
var auto = false;
var wallWrap = true;
var score = 0;

function drawSquare(x, y, c) {
  stroke(c);
  fill(c);
  rect(x * gridSquareWidth - gridSquareWidth,
    y * gridSquareWidth - gridSquareWidth,
    gridSquareWidth - margin, gridSquareWidth - margin)
}

function newApple() {
  return createVector(Math.floor(random(gridWidth - 1) + 1),
    Math.floor(random(gridHeight - 1) + 1));
}

function update() {
  gridSquareWidth = parseInt(document.getElementById("gridSquareSize").value);
  if (gridSquareWidth <= 0) {
    gridsquareWidth = 1;
  }
  margin = parseInt(document.getElementById("marginSize").value);
  startLength = parseInt(document.getElementById("startlength").value);
  lengthInc = parseInt(document.getElementById("lengthInc").value);
  fps = parseInt(document.getElementById("fps").value);
  auto = document.getElementById("auto").checked;
  wallWrap = document.getElementById("wallWrap").checked;
}

function reset() {
  setup();
    snakeHead = createVector(Math.floor((gridWidth - 1)/2),
        Math.floor((gridHeight - 1)/2));
  apple = newApple();
  snakeTail = [];
  snakeLength = startLength;
}


function setup() {
  // createCanvas(windowWidth - (windowWidth % gridSquareWidth), windowHeight - (windowHeight % gridSquareWidth));
  update();
  createCanvas(windowWidth, windowHeight);
  background(50);
  gridWidth = Math.floor(width / gridSquareWidth);
  gridHeight = Math.floor(height / gridSquareWidth);

  snakeHead = createVector(Math.floor(random(gridWidth - 1)),
    Math.floor(random(gridHeight - 1)));
  apple = createVector(Math.floor(random(gridWidth - 1)),
    Math.floor(random(gridHeight - 1)));
  drawSquare(snakeHead.x, snakeHead.y, headColor);
  drawSquare(apple.x, apple.y, appleColor);
  fill(50);
  frameRate(fps);
}

function gameOver() {
  document.getElementById("scoreOutput").innerHTML = "You finished with a score of " + score;
  document.getElementById("scoreBox").style.opacity = "1";
  document.getElementById("options").style.opacity = "1";
  document.getElementById("options").style.background = "none";
  document.getElementById("updateButton").style.opacity = "0";
  playing = false;
  reset();
}

function updateAndReset() {
  update();
  reset();
}

function startGame() {
  document.getElementById("scoreBox").style.opacity = "";
  document.getElementById("options").style.opacity = "";
  document.getElementById("updateButton").style.opacity = "";
  document.getElementById("options").style.background = "";
  updateAndReset();
  playing = true;
}

function notCollide(x, y) {
  if (wallWrap === false){
    if (x <= 0 || x > gridWidth || y <= 0 || y > gridHeight) {
      return false;
    }
  }
  for (let i in snakeTail) {
    if (x == snakeTail[i].x && y == snakeTail[i].y) {
      return false;
    }
  }
  return true;
}




function draw() {
  if (playing) {
    background(0, 0, 0, 1);
    fill(50);
    stroke(50);
    rect(0, 0, gridWidth * gridSquareWidth, gridHeight * gridSquareWidth);
    snakeTail.push(snakeHead.copy());
    drawSquare(apple.x, apple.y, appleColor);

    if (auto) {
      if (apple.x < snakeHead.x && previous_direction != "right" && notCollide(snakeHead.x - 1, snakeHead.y)) {
        direction = "left";
      } else if (apple.x > snakeHead.x && previous_direction != "left" && notCollide(snakeHead.x + 1, snakeHead.y)) {
          direction = "right";
        } else if (apple.y < snakeHead.y && previous_direction != "down" && notCollide(snakeHead.x, snakeHead.y - 1)) {
            direction = "up";
          } else if (apple.y > snakeHead.y && previous_direction != "up" && notCollide(snakeHead.x, snakeHead.y + 1)) {
              direction = "down";
            } else if (notCollide(snakeHead.x - 1, snakeHead.y)) {
                direction = "left";
              } else if (notCollide(snakeHead.x + 1, snakeHead.y)) {
                  direction = "right";
                } else if (notCollide(snakeHead.x, snakeHead.y + 1)) {
                    direction = "down";
                  } else if (notCollide(snakeHead.x, snakeHead.y - 1)) {
                      direction = "up";
                    }
                  }
                  switch (direction) {
                    case "up":
                      if (previous_direction == "down") {
                        direction = "down";
                        snakeHead.y += 1;
                      } else {
                        snakeHead.y -= 1;
                      }
                      break;
                    case "right":
                      if (previous_direction == "left") {
                        direction = "left";
                        snakeHead.x -= 1;
                      } else {
                        snakeHead.x += 1;
                      }
                      break;
                    case "down":
                      if (previous_direction == "up") {
                        direction = "up";
                        snakeHead.y -= 1;
                      } else {
                        snakeHead.y += 1;
                      }
                      break;
                    case "left":
                      if (previous_direction == "right") {
                        direction = "right";
                        snakeHead.x += 1
                      } else {
                        snakeHead.x -= 1
                      }
                      break;
                  }
                  previous_direction = direction;
                  if (wallWrap) {
                    if (snakeHead.x <= 0) {
                      snakeHead.x = gridWidth;
                    }
                    else if (snakeHead.x > gridWidth) {
                      snakeHead.x = 1;
                    }
                    else if (snakeHead.y <= 0) {
                      snakeHead.y = gridHeight;
                    }
                    else if (snakeHead.y > gridHeight) {
                      snakeHead.y = 1;
                    }
                  }
    else if (snakeHead.x <= 0 || snakeHead.x > gridWidth || snakeHead.y <= 0 || snakeHead.y > gridHeight) {
                    gameOver();
                  }

                  if (snakeLength-1 < snakeTail.length) {
                    snakeTail.shift();
                  }
                  for (let i in snakeTail) {
                    if (snakeHead.x == snakeTail[i].x && snakeHead.y == snakeTail[i].y) {
                      gameOver();
                      break;
                    }

                    drawSquare(snakeTail[i].x, snakeTail[i].y, snakeColor);
                  }
                  drawSquare(snakeHead.x, snakeHead.y, headColor);
                  if (snakeHead.x == apple.x && snakeHead.y == apple.y) {
                    snakeLength += lengthInc;
                    score += 1;
                    apple = newApple();
                  }
                  for (let i in snakeTail) {
                    if (apple.x == snakeTail[i].x && apple.y == snakeTail[i].y) {
                      snakeLength += lengthInc;
                      apple = newApple();
                    }

                  }
                }

              }

              function keyPressed() {
                if (keyCode == 65) {
                  auto = !auto;
                }
                if (auto === false) {
                  if (keyCode === LEFT_ARROW) {
                    direction = "left";
                  } else if (keyCode === RIGHT_ARROW) {
                    direction = "right";
                  } else if (keyCode === UP_ARROW) {
                    direction = "up";
                  } else if (keyCode === DOWN_ARROW) {
                    direction = "down";
                  }
                }
              }
